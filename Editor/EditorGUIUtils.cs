using UnityEngine;
using UnityEditor;

public static class EditorGUIUtils {
    public static Color BackgroundColor => EditorGUIUtility.isProSkin
        ? new Color32(56, 56, 56, 255)
        : new Color32(194, 194, 194, 255);
}
