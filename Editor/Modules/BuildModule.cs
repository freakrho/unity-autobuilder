﻿using UnityEditor;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Autobuilder.ReorderableList;
using System;

namespace Autobuilder {
    public abstract class BuildModule {
        public const string BASE_DIR = "ProjectSettings/BuildModules";
        const string DEFINE_SYMBOLS = "DefineSymbols";
        const string SCENES = "Scenes";
        const string BUILD_NUMBER = "BuildNumber";
        const string SORTING_INDEX = "SortingIndex";
        const string ASSET_BUNDLE_OPTIONS = "AssetBundleOptions";
        const int ARROWS_WIDTH = 64;
        const int ARROWS_HEIGHT = 40;
        public const string GUID = "GUID";
        public const string ADDED = "Added";
        public const string PRE_BUILD_SCRIPTS = "PreBuildScripts";
        public const string POST_BUILD_SCRIPTS = "PostBuildScripts";

        static Texture arrowUp;
        static Texture arrowDown;
        static Texture ArrowUp {
            get {
                if (arrowUp == null) {
                    arrowUp = Resources.Load<Texture>("little_arrow_up");
                }
                return arrowUp;
            }
        }
        static Texture ArrowDown {
            get {
                if (arrowDown == null) {
                    arrowDown = Resources.Load<Texture>("little_arrow_down");
                }
                return arrowDown;
            }
        }
        static GUIContent arrowUpContent;
        static GUIContent ArrowUpContent {
            get {
                if (arrowUpContent == null) {
                    arrowUpContent = new GUIContent(ArrowUp);
                }
                return arrowUpContent;
            }
        }
        static GUIContent arrowDownContent;
        static GUIContent ArrowDownContent {
            get {
                if (arrowDownContent == null) {
                    arrowDownContent = new GUIContent(ArrowDown);
                }
                return arrowDownContent;
            }
        }
        static Texture2D buildToggleOff;
        static Texture2D BuildToggleOff {
            get {
                if (buildToggleOff == null) {
                    buildToggleOff = Resources.Load<Texture2D>("toggle_off");
                }
                return buildToggleOff;
            }
        }
        static Texture2D buildToggleOn;
        static Texture2D BuildToggleOn {
            get {
                if (buildToggleOn == null) {
                    buildToggleOn = Resources.Load<Texture2D>("toggle_build");
                }
                return buildToggleOn;
            }
        }
        static GUIStyle buildToggle;
        static GUIStyle BuildToggle {
            get {
                if (buildToggle == null) {
                    buildToggle = new GUIStyle(GUI.skin.toggle);
                    buildToggle.normal.background = BuildToggleOff;
                    buildToggle.onNormal.background = BuildToggleOn;
                }
                return buildToggle;
            }
        }
        static GUIStyle overflowLabel;
        static GUIStyle OverflowLabel {
            get {
                if (overflowLabel == null) {
                    overflowLabel = new GUIStyle(GUI.skin.label);
                    overflowLabel.wordWrap = true;
                    overflowLabel.alignment = TextAnchor.MiddleCenter;
                }
                return overflowLabel;
            }
        }

        public abstract BuildTarget Target { get; }
        public abstract BuildTargetGroup TargetGroup { get; }
        public abstract bool IsTarget(BuildTarget aTarget);
        string EnabledKey => $"{Builder.BUILDER}.{ModuleName}.Enabled";
        public bool Enabled {
            get { return PlayerPrefs.GetInt(EnabledKey, 0) > 0; }
            private set { PlayerPrefs.SetInt(EnabledKey, value ? 1 : 0); }
        }
        public int SortingIndex {
            get { return GetInt(SORTING_INDEX, 0); }
            set { SetInt(SORTING_INDEX, value); }
        }
        public int BuildNumber {
            get { return GetInt(BUILD_NUMBER, 0); }
            set { SetInt(BUILD_NUMBER, value); }
        }
        protected string BaseBuildPath => $"{Builder.DataPath}/{Builder.BuildPath}/{ModuleName}";
        string GUIUnfoldKey => $"{Builder.BUILDER}.Unfold.{ModuleName}";
        bool GUIUnfold {
            get { return PlayerPrefs.GetInt(GUIUnfoldKey, 0) > 0; }
            set { PlayerPrefs.SetInt(GUIUnfoldKey, value ? 1 : 0); }
        }
        BuildAssetBundleOptions AssetBundleOptions {
            get { return (BuildAssetBundleOptions) GetInt(ASSET_BUNDLE_OPTIONS); }
            set { SetInt(ASSET_BUNDLE_OPTIONS, (int) value); }
        }
        bool UnfoldScenes {
            get => EditorPrefs.GetBool($"{ModuleName}_Scenes_Fold");
            set { EditorPrefs.SetBool($"{ModuleName}_Scenes_Fold", value); }
        }
        bool UnfoldDefines {
            get => EditorPrefs.GetBool($"{ModuleName}_Defines_Fold");
            set { EditorPrefs.SetBool($"{ModuleName}_Defines_Fold", value); }
        }
        bool UnfoldBuildScripts {
            get => EditorPrefs.GetBool($"{ModuleName}_BuildScripts_Fold");
            set { EditorPrefs.SetBool($"{ModuleName}_BuildScripts_Fold", value); }
        }

        public void OnGUI(out bool build, out bool development) {
            var isTarget = IsTarget(EditorUserBuildSettings.activeBuildTarget);

            GUILayout.BeginHorizontal();

            GUILayout.BeginVertical(GUILayout.Width(ARROWS_WIDTH));
            if (SortingIndex > 0) {
                if (GUILayout.Button(ArrowUpContent, GUILayout.Height(ARROWS_HEIGHT))) {
                    SortingIndex -= 3;
                }
            } else {
                GUILayout.Space(ARROWS_WIDTH);
            }
            if (Builder.TargetModuleInstalled(Target)) {
                Enabled = EditorGUILayout.Toggle(Enabled, BuildToggle,
                    GUILayout.Width(ARROWS_WIDTH), GUILayout.Height(ARROWS_WIDTH));
            } else {
                GUILayout.Label($"Module {Target} not installed", OverflowLabel,
                    GUILayout.Height(ARROWS_WIDTH), GUILayout.Width(ARROWS_WIDTH));
            }
            if (GUILayout.Button(ArrowDownContent, GUILayout.Height(ARROWS_HEIGHT))) {
                SortingIndex += 3;
            }
            GUILayout.EndVertical();

            // TODO: Change color if enabled/disabled
            GUILayout.BeginVertical(isTarget ? Builder.SelectedAreaStyle : Builder.AreaStyle);

            EditorGUI.BeginChangeCheck();
            var moduleName = EditorGUILayout.DelayedTextField(ModuleName);
            var buildNumber = EditorGUILayout.DelayedIntField("Build number", BuildNumber);
            if (EditorGUI.EndChangeCheck()) {
                bool enabled = Enabled;
                PlayerPrefs.DeleteKey(EnabledKey);
                ModuleName = moduleName;
                Enabled = enabled;
                BuildNumber = buildNumber;
            }
            GUILayout.BeginHorizontal();
            GUILayout.Label($"Build Target Group: {TargetGroup}");
            GUILayout.Label($"Build Target: {Target}");
            GUILayout.EndHorizontal();
            EditorGUI.BeginChangeCheck();

            var rect = GUILayoutUtility.GetRect(new GUIContent(""), ReorderableListStyles.Title);
            ReorderableListGUI.Title(rect, "");
            UnfoldScenes = EditorGUI.Foldout(rect, UnfoldScenes, "Scenes");
            if (UnfoldScenes) {
                ReorderableListGUI.ListField(m_ScenesAdaptor);
            }
            var defineTitle = new GUIContent("Define Symbols");
            rect = GUILayoutUtility.GetRect(defineTitle, ReorderableListStyles.Title);
            ReorderableListGUI.Title(rect, "");
            UnfoldDefines = EditorGUI.Foldout(rect, UnfoldDefines, defineTitle);
            if (UnfoldDefines) {
                ReorderableListGUI.ListField(m_DefineSymbolsAdaptor);
            }
            
            // DefineSymbols
            rect.x = rect.x + rect.width * .5f;
            rect.width *= .5f;
            rect.y += 1;
            rect.height -= 2;

            if (GUI.Button(rect, "Copy Current Define Symbols")) {
                CopyCurrentSymbols();
            }

            var buildScriptsTitle = new GUIContent("Build Scripts");
            rect = GUILayoutUtility.GetRect(buildScriptsTitle, ReorderableListStyles.Title);
            ReorderableListGUI.Title(rect, "");
            UnfoldBuildScripts = EditorGUI.Foldout(rect, UnfoldBuildScripts, buildScriptsTitle);
            if (UnfoldBuildScripts) {
                ReorderableListGUI.Title("Pre build scripts");
                ReorderableListGUI.ListField(m_PreBuildScriptsAdaptor);
                ReorderableListGUI.Title("Post build scripts");
                ReorderableListGUI.ListField(m_PostBuildScriptsAdaptor);
            }

            // Asset bundle options
            AssetBundleOptions = (BuildAssetBundleOptions) EditorGUILayout.EnumFlagsField("Asset Bundle Options", AssetBundleOptions);

            // Options
            var optionsTitle = new GUIContent("Options");
            rect = GUILayoutUtility.GetRect(optionsTitle, ReorderableListStyles.Title);
            ReorderableListGUI.Title(rect, "");
            var unfold = EditorGUI.Foldout(rect, GUIUnfold, optionsTitle);
            if (EditorGUI.EndChangeCheck()) {
                GUIUnfold = unfold;
                PlayerPrefs.Save();
            }
            if (unfold) {
                OptionsGUI(out build, out development);
            } else {
                build = false;
                development = false;
            }

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

        public abstract void OptionsGUI(out bool aBuild, out bool aDevelopment);

        /// <summary>
        /// Builds the game
        /// </summary>
        /// <returns>True if successfull</returns>
        public virtual bool BuildGame(bool development = false) {
            if (!Enabled) return false;

            SetDefineSymbols();

            return true;
        }

        // Data
        string FileName => $"{BASE_DIR}/{ModuleName}.bm";
        public string ModuleName;
        public string ModuleTarget => $"{TargetGroup}.{Target}";
        protected JObject Data;
        protected JArray DefineSymbols;
        protected JArray Scenes;
        protected JArray PreBuildScripts;
        protected JArray PostBuildScripts;
        JSONNodeAdaptor m_DefineSymbolsAdaptor;
        SceneListAdaptor m_ScenesAdaptor;
        ScriptListAdaptor m_PreBuildScriptsAdaptor;
        ScriptListAdaptor m_PostBuildScriptsAdaptor;

        public BuildModule() {
            Load(new JObject());
        }

        JArray GetArray(JObject root, string key) {
            var node = root[key];
            JArray array;
            if (node == null || !(node is JArray)) {
                array = new JArray();
                root[key] = array;
            } else {
                array = node as JArray;
            }
            return array;
        }

        public virtual void Load(JObject root) {
            Data = root;
            DefineSymbols = GetArray(Data, DEFINE_SYMBOLS);
            Scenes = GetArray(Data, SCENES);
            PreBuildScripts = GetArray(Data, PRE_BUILD_SCRIPTS);
            PostBuildScripts = GetArray(Data, POST_BUILD_SCRIPTS);
            m_DefineSymbolsAdaptor = new JSONNodeAdaptor(DefineSymbols);
            m_DefineSymbolsAdaptor.ForcedType = JTokenType.String;
            m_ScenesAdaptor = new SceneListAdaptor(Scenes);
            m_PreBuildScriptsAdaptor = new ScriptListAdaptor(PreBuildScripts);
            m_PostBuildScriptsAdaptor = new ScriptListAdaptor(PostBuildScripts);
        }

        public virtual void Load() {
            var text = File.ReadAllText(FileName);
            Load(JObject.Parse(text));
        }

        public void Save() {
            Data["ModuleTarget"] = ModuleTarget;
            File.WriteAllText(FileName, Data.ToString(Formatting.Indented));
        }

        protected bool GetBool(string name, bool defaultValue = false) {
            var node = Data[name];
            if (node == null) return defaultValue;
            return (bool)node;
        }

        protected void SetBool(string name, bool value) {
            Data[name] = value;
        }

        protected float GetFloat(string name, float defaultValue = 0) {
            var node = Data[name];
            if (node == null) return defaultValue;
            return (float)node;
        }

        protected void SetFloat(string name, float value) {
            Data[name] = value;
        }

        protected int GetInt(string name, int defaultValue = 0) {
            var node = Data[name];
            if (node == null) return defaultValue;
            return (int)node;
        }

        protected void SetInt(string name, int value) {
            Data[name] = value;
        }

        protected string GetString(string name, string defaultValue) {
            var node = Data[name];
            if (node == null) return defaultValue;
            return (string)node;
        }

        protected void SetString(string name, string value) {
            Data[name] = value;
        }

        public void SetDefineSymbols() {
            string defines = "";
            foreach (var item in DefineSymbols) {
                defines += item + ";";
            }
            PlayerSettings.SetScriptingDefineSymbolsForGroup(TargetGroup, defines);
        }

        protected string[] GetScenesList() {
            var scenes = new List<string>();

            for (int i = 0; i < EditorBuildSettings.scenes.Length; i++) {
                if (EditorBuildSettings.scenes[i].enabled)
                    scenes.Add(EditorBuildSettings.scenes[i].path);
            }
            // Modify list using specific scenes
            foreach (var scene in Scenes) {
                var added = (bool) scene[ADDED];
                var path = AssetDatabase.GUIDToAssetPath((string) scene[GUID]);
                if (added) {
                    scenes.Add(path);
                } else {
                    for (int i = scenes.Count - 1; i >= 0; i--) {
                        if (scenes[i] == path) {
                            scenes.RemoveAt(i);
                        }
                    }
                }
            }
            // foreach (var scene in scenes) Debug.Log(scene);
            return scenes.ToArray();
        }

        public virtual void BuildAssetBundles() {
            Builder.BuildAssetBundles(Target);
        }

        public void CopyCurrentSymbols() {
            PlayerSettings.GetScriptingDefineSymbolsForGroup(TargetGroup, out var symbols);
            DefineSymbols.Clear();
            foreach (var symbol in symbols) {
                DefineSymbols.Add(symbol);
            }
        }

        public void Remove() {
            File.Delete(FileName);
        }
    }

    public class SceneListAdaptor : IReorderableListAdaptor {
        enum SceneState {
            Equal,
            Added,
            Removed,
        }

        class SceneData {
            public string Guid;
            public string Name;
            public SceneState State;
        }

        JArray list;
        List<SceneData> sceneList;
        List<SceneData> SceneList {
            get {
                if (sceneList == null) {
                    CreateList();
                }
                return sceneList;
            }
            set { sceneList = value; }
        }

        public SceneListAdaptor(JArray scenes) {
            list = scenes;
            EditorBuildSettings.sceneListChanged += OnSceneListChange;
        }

        private void OnSceneListChange() {
            CreateList();
        }

        public void CreateList() {
            SceneList = new List<SceneData>();
            
            foreach (var scene in EditorBuildSettings.scenes) {
                if (!scene.enabled) continue;
                SceneList.Add(new SceneData {
                    Guid = scene.guid.ToString(),
                    State = SceneState.Equal,
                    Name = scene.path,
                });
            }
            
            var removeNodes = new List<JObject>();
            foreach (JObject node in list) {
                var data = new SceneData();
                var guid = node[BuildModule.GUID].Value<string>();
                data.Guid = guid;
                data.Name = AssetDatabase.GUIDToAssetPath(guid);
                if (node[BuildModule.ADDED].Value<bool>()) {
                    if (SceneInBuildSettings(guid)) {
                        removeNodes.Add(node);
                        continue;
                    }
                    data.State = SceneState.Added;
                    SceneList.Add(data);
                } else { // removed
                    if (!SceneInBuildSettings(guid)) {
                        removeNodes.Add(node);
                        continue;
                    } else {
                        data.State = SceneState.Removed;

                        int index = SceneInList(guid.ToString());
                        if (index >= 0) {
                            SceneList.RemoveAt(index);
                            SceneList.Insert(index, data);
                        } else {
                            SceneList.Add(data);
                        }
                    }
                }
            }

            foreach (var node in removeNodes) {
                node.Remove();
            }
        }

        int SceneInList(string guid) {
            for (int i = 0; i < SceneList.Count; i++) {
                if (SceneList[i].Guid == guid) return i;
            }
            return -1;
        }

        bool SceneInBuildSettings(string guid) {
            foreach (var scene in EditorBuildSettings.scenes) {
                if (scene.guid.ToString() == guid) return true;
            }
            return false;
        }

        public int Count => SceneList.Count;

        public void Add() {
            var filePath = EditorUtility.OpenFilePanel("Choose a scene", "Assets", "unity");
            if (File.Exists(filePath)) {
                filePath = Path.GetRelativePath(
                    Application.dataPath.Substring(0, Application.dataPath.Length - "Assets/".Length),
                    filePath
                );
                var scene = AssetDatabase.LoadAssetAtPath<SceneAsset>(filePath);
                if (scene != null) {
                    var guid = AssetDatabase.AssetPathToGUID(filePath);
                    SceneList.Add(new SceneData {
                        Guid = guid,
                        State = SceneState.Added,
                        Name = filePath,
                    });
                    JToken node = new JObject();
                    node[BuildModule.GUID] = guid;
                    node[BuildModule.ADDED] = true;
                    list.Add(node);
                }
            }
        }

        public void BeginGUI() {
        }

        public bool CanDrag(int index) => false;

        public bool CanRemove(int index) => true;

        public void Clear() {
            list.Clear();
            CreateList();
        }

        public void DrawItem(Rect position, int index) {
            var scene = SceneList[index];
            string symbol;
            switch (scene.State) {
                case SceneState.Added:
                    symbol = "+";
                    break;
                case SceneState.Removed:
                    symbol = "-";
                    break;
                default:
                    symbol = "=";
                    break;
            }
            EditorGUI.LabelField(position, $"{symbol} {scene.Name}");
        }

        public void DrawItemBackground(Rect position, int index) {
            var scene = SceneList[index];
            Color color;
            switch (scene.State) {
                case SceneState.Added:
                    color = Color.green;
                    break;
                case SceneState.Removed:
                    color = Color.red;
                    break;
                default:
                    return;
            }
            GUISkin skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
            EditorGUI.DrawRect(position, Color.Lerp(color, EditorGUIUtils.BackgroundColor, 0.85f));
        }

        public void Duplicate(int index) {
        }

        public void EndGUI() {
        }

        public float GetItemHeight(int index) {
            return EditorGUIUtility.singleLineHeight;
        }

        public void Insert(int index) {
        }

        public void Move(int sourceIndex, int destIndex) {
            
        }

        public void Remove(int index) {
            var scene = SceneList[index];
            switch (scene.State) {
                case SceneState.Equal: {
                    // Add removed scene
                    JToken node = new JObject();
                    node[BuildModule.GUID] = scene.Guid;
                    node[BuildModule.ADDED] = false;
                    list.Add(node);
                    scene.State = SceneState.Removed;
                    break;
                }
                case SceneState.Added: {
                    // Remove added
                    for (int i = 0; i < list.Count; i++) {
                        var item = list[i];
                        if (item[BuildModule.GUID].Value<string>() == scene.Guid) {
                            item.Remove();
                            break;
                        }
                    }
                    SceneList.RemoveAt(index);
                    break;
                }
                case SceneState.Removed: {
                    // Restore removed
                    for (int i = 0; i < list.Count; i++) {
                        var item = list[i];
                        if (item[BuildModule.GUID].Value<string>() == scene.Guid) {
                            item.Remove();
                            break;
                        }
                    }
                    scene.State = SceneState.Equal;
                    break;
                }
            }
        }
    }

    class ScriptListAdaptor : IReorderableListAdaptor {
        JArray list;
        List<Dictionary<string, JSONFilesAdaptor>> adaptors;

        public int Count => list.Count;

        public ScriptListAdaptor(JArray array) {
            list = array;
            adaptors = new List<Dictionary<string, JSONFilesAdaptor>>();
            for (int i = 0; i < list.Count; i++) {
                adaptors.Add(new Dictionary<string, JSONFilesAdaptor>());
            }
        }

        JObject New() {
            var node = new JObject();
            node[Builder.WINDOWS] = new JArray();
            node[Builder.LINUX] = new JArray();
            node[Builder.OSX] = new JArray();
            return node;
        }

        public void Add() {
            list.Add(New());
            adaptors.Add(new Dictionary<string, JSONFilesAdaptor>());
        }

        public void BeginGUI() {}
        public bool CanDrag(int index) => true;
        public bool CanRemove(int index) => true;

        public void Clear() {
            list.Clear();
        }

        JSONFilesAdaptor GetAdaptor(int index, string platform) {
            var item = (JObject) list[index];
            if (!item.ContainsKey(platform)) {
                item[platform] = new JArray();
            }
            var adaptorDict = adaptors[index];
            if (!adaptorDict.ContainsKey(platform)) {
                adaptorDict.Add(platform, new JSONFilesAdaptor((JArray) item[platform]));
            }
            return adaptorDict[platform];
        }

        void DrawPlatformList(ref Rect position, int index, string title, string key) {
            position.height = ReorderableListGUI.DefaultItemHeight;
            ReorderableListGUI.Title(position, title);
            position.y += position.height;
            var adaptor = GetAdaptor(index, key);
            position.height = ReorderableListGUI.CalculateListFieldHeight(adaptor);
            ReorderableListGUI.ListFieldAbsolute(position, adaptor);
            position.y += position.height;
        }

        public void DrawItem(Rect position, int index) {
            DrawPlatformList(ref position, index, "Windows", Builder.WINDOWS);
            DrawPlatformList(ref position, index, "Linux", Builder.LINUX);
            DrawPlatformList(ref position, index, "OSX", Builder.OSX);
        }

        public void DrawItemBackground(Rect position, int index) {}

        public void Duplicate(int index) {
            var item = list[index];
            Insert(index, item);
        }

        public void EndGUI() {}

        public float GetItemHeight(int index) {
            var height = ReorderableListGUI.DefaultItemHeight * 3; // titles
            JSONFilesAdaptor adaptor;
            // windows
            adaptor = GetAdaptor(index, Builder.WINDOWS);
            height += ReorderableListGUI.CalculateListFieldHeight(adaptor);
            // linux
            adaptor = GetAdaptor(index, Builder.LINUX);
            height += ReorderableListGUI.CalculateListFieldHeight(adaptor);
            // osx
            adaptor = GetAdaptor(index, Builder.OSX);
            height += ReorderableListGUI.CalculateListFieldHeight(adaptor);
            return height;
        }

        void Insert(int index, JToken item) {
            Insert(index, item, new Dictionary<string, JSONFilesAdaptor>());
        }

        void Insert(int index, JToken item, Dictionary<string, JSONFilesAdaptor> adaptorDict) {
            list.Insert(index, item);
            adaptors.Insert(index, adaptorDict);
        }

        public void Insert(int index) {
            Insert(index, New());
        }

        public void Move(int sourceIndex, int destIndex) {
            var item = list[sourceIndex];
            var adaptorDict = adaptors[sourceIndex];
            if (sourceIndex > destIndex) {
                Remove(sourceIndex);
                Insert(destIndex, item, adaptorDict);
            } else {
                Insert(destIndex, item, adaptorDict);
                Remove(sourceIndex);
            }
        }

        public void Remove(int index) {
            list.RemoveAt(index);
            adaptors.RemoveAt(index);
        }
    }
}
