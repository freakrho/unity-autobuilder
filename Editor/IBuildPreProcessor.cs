using UnityEditor;

namespace Autobuilder
{
    public interface IBuildPreProcessor
    {
        void PreProcess(BuildTarget aTarget, bool aDevelopment);
    }
}
